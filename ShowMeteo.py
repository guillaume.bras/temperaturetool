#!/usr/bin/env python

"""Test d'ecriture depuis python

%s
"""

__usage__ = """
 usage: python TestWeatherStation.py [options]
 options are:
       --help           display this help
  -d | --decode         display meaningful values instead of raw data
  -h | --history count  display the last "count" readings
  -l | --live           display 'live' data
  -m | --logged         display 'logged' data
  -u | --unknown        display unknown fixed block values
  -v | --verbose        increase amount of reassuring messages
                        (repeat for even more messages e.g. -vvv)
"""

__doc__ %= __usage__

__usage__ = __doc__.split('\n')[0] + __usage__

import datetime, time
import getopt
import sys

from pywws.Logger import ApplicationLogger
from pywws import WeatherStation

from numpy import *

# If the package has been installed correctly, this should work:
import Gnuplot, Gnuplot.funcutils, sys


def myplot(filename):
    """Demonstrate the Gnuplot package."""

    # A straightforward use of gnuplot.  The `debug=1' switch is used
    # in these examples so that the commands that are sent to gnuplot
    # are also output on stderr.
    g = Gnuplot.Gnuplot(debug=1)
    g.title('Current temp / hum') # (optional)
    g('set datafile separator ","') # give gnuplot an arbitrary command
    g('set xdata time')
    g('set timefmt "%s"')
    g('set format x "%a-%Hh"')
    g('set xlabel "time"')
    g('set grid xtics')
    g('set grid ytics')
    g('set yrange [10:24]') # avant 16:24
    g('set y2range [40:90]')
    g("set y2tics 10 nomirror tc lt 2")
    g("set ylabel 'temp'")
    g("set y2label 'humidity'")
    # Plot a list of (x, y) pairs (tuples or a numpy array would
    # also be OK):
    g.plot('"' + filename + '" using 1:3 with lines, "'+filename+'" using 1:4 axes x1y2 with lines')
    raw_input('Please press return to continue...\n')

    g.reset()


def raw_dump(pos, data):
    print "%04x" % pos,
    for item in data:
        print "%02x" % item,
    print

def raw_interprete(pos, data, meta, verbose = False):
    if pos == 0:
        delay = data[1]
        if verbose: print "Delay = ", data[1], " minutes"
        current = data[2]*256
        if verbose: print "Current block at = ", current, "%04x" % current
        meta['delay'] = delay
        meta['current'] = current
    elif pos == 0x20:
        pass
    elif pos >= 0x100:
        i=0
        for el in data:
            if not('last_record' in meta) and el == 0xff:
                meta['last_record'] = pos +i -4
                if verbose: print "end of block !", meta['last_record'], "%04x" % meta['last_record']
                break
            i+=1
        
    
def raw_dump_better(pos, data, verbose = False, direct = False):
    if verbose: print "%04x" % pos,
    i=0
    for item in data:
        if i==0:
            result = {}
            debut = item
        elif i==1:
            temp = ((debut*256 + item) - 400)/10.
            tempf = "%02.01f" % temp
            if verbose: print tempf
        elif i ==2:
            if verbose: print "%03d%%" % item,
            humidity = item
        else: 
            if verbose: print "%02xm" % item,
            delay = item
            result['temp'] = tempf
            result['delay'] = delay
            result['humidity'] = humidity
            if direct: return result
        i=(i+1)%4
    print
def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        opts, args = getopt.getopt(
            argv[1:], "dh:lmuv",
            ('help', 'decode', 'history=', 'live', 'logged', 'unknown', 'verbose'))
    except getopt.error, msg:
        print >>sys.stderr, 'Error: %s\n' % msg
        print >>sys.stderr, __usage__.strip()
        return 1
    # check arguments
    if len(args) != 0:
        print >>sys.stderr, 'Error: no arguments allowed\n'
        print >>sys.stderr, __usage__.strip()
        return 2
    # process options
    history_count = 0
    decode = False
    live = False
    logged = False
    unknown = False
    verbose = 0
    for o, a in opts:
        if o == '--help':
            print __usage__.strip()
            return 0
        elif o in ('-d', '--decode'):
            decode = True
        elif o in ('-h', '--history'):
            history_count = int(a)
        elif o in ('-l', '--live'):
            live = True
            logged = False
        elif o in ('-m', '--logged'):
            live = False
            logged = True
        elif o in ('-u', '--unknown'):
            unknown = True
        elif o in ('-v', '--verbose'):
            verbose += 1
    # do it!
    logger = ApplicationLogger(verbose)
    ws = WeatherStation.weather_station()
    raw_fixed = ws.get_raw_fixed_block()
    if not raw_fixed:
        print "No valid data block found"
        return 3

    meta = {}
    for ptr in range(0x0000, 0x200, 0x20):
        if (ptr >= 0x100):
            raw_dump_better(ptr, raw_fixed[ptr:ptr+0x20])
        else:
            raw_interprete(ptr, raw_fixed[ptr:ptr+0x20], meta)
    current_block = ws._read_custom_block(meta['current'])
    for ptr in range(0x0000, 0x100, 0x20):
        raw_interprete(meta['current'] + ptr, current_block[ptr:ptr+0x20], meta)
    print "############################"
    print "last record = ", meta['last_record']
    date = datetime.datetime.now().replace(second=0, microsecond=0)

    history = []
    l = range(0x100, meta['last_record'], 4)
    l.reverse()
    nb = len(l)
    i=0
    for ptr in l:#range(meta['last_record'], 0x100, 4):
        data = ws.get_raw_data(ptr)
        res = raw_dump_better(ptr, data, False, True)
        #print "-debug:",  res, len(history), len(l)
        res['date'] = date.strftime("%d/%m %H-%M")
        res['timestamp'] = time.mktime(date.timetuple())
        history.append(res)
        date = date - datetime.timedelta(minutes=res['delay'])
        i+=1
        sys.stdout.write("\r" + str(i) + ' / ' +str( nb))
    history.reverse()
    filename = datetime.datetime.now().strftime("donnees%Y%m%d.csv")
    f = open(filename, "w")
    for i in history:
        line=  str(i['timestamp'])+',"'+str(i['date'])+'",'+str(i['temp'])+','+str(i['humidity'])
        #print line
        f.write(line + "\n")
    f.close()
    del ws
    
    print "done ! "
    
    


    myplot(filename)
    
    
    
if __name__ == "__main__":
    try:
        sys.exit(main())
    except KeyboardInterrupt:
        pass
