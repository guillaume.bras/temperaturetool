#!/usr/bin/env python

"""Test connection to weather station.

This is a simple utility to test communication with the weather
station. If this doesn't work, then there's a problem that needs to be
sorted out before trying any of the other programs. Likely problems
include not properly installing `libusb
<http://libusb.wiki.sourceforge.net/>`_ or `PyUSB
<http://pyusb.berlios.de/>`_. Less likely problems include an
incompatibility between libusb and some operating systems. The most
unlikely problem is that you forgot to connect the weather station to
your computer! ::


%s
"""

__usage__ = """
 usage: python TestWeatherStation.py [options]
 options are:
       --help           display this help
  -d | --decode         display meaningful values instead of raw data
  -h | --history count  display the last "count" readings
  -l | --live           display 'live' data
  -m | --logged         display 'logged' data
  -u | --unknown        display unknown fixed block values
  -v | --verbose        increase amount of reassuring messages
                        (repeat for even more messages e.g. -vvv)
"""

__doc__ %= __usage__

__usage__ = __doc__.split('\n')[0] + __usage__

import datetime, time
import getopt
import sys

from pywws.Logger import ApplicationLogger
from pywws import WeatherStation

def raw_dump(pos, data):
    print "%04x" % pos,
    for item in data:
        print "%02x" % item,
    print

def raw_interprete(pos, data, meta, verbose = False):
    if pos == 0:
        delay = data[1]
        if verbose: print "Delay = ", data[1], " minutes"
        current = data[2]*256
        if verbose: print "Current block at = ", current, "%04x" % current
        meta['delay'] = delay
        meta['current'] = current
    elif pos == 0x20:
        pass
    elif pos >= 0x100:
        i=0
        for el in data:
            if not('last_record' in meta) and el == 0xff:
                meta['last_record'] = pos +i -4
                if verbose: print "end of block !", meta['last_record'], "%04x" % meta['last_record']
                break
            i+=1
        
    
def raw_dump_better(pos, data, verbose = False, direct = False):
    if verbose: print "%04x" % pos,
    i=0
    for item in data:
        if i==0:
            result = {}
            debut = item
        elif i==1:
            temp = ((debut*256 + item) - 400)/10.
            tempf = "%02.01f" % temp
            if verbose: print tempf
        elif i ==2:
            if verbose: print "%03d%%" % item,
            humidity = item
        else: 
            if verbose: print "%02xm" % item,
            delay = item
            result['temp'] = tempf
            result['delay'] = delay
            result['humidity'] = humidity
            if direct: return result
        i=(i+1)%4
    print
def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        opts, args = getopt.getopt(
            argv[1:], "dh:lmuv",
            ('help', 'decode', 'history=', 'live', 'logged', 'unknown', 'verbose'))
    except getopt.error, msg:
        print >>sys.stderr, 'Error: %s\n' % msg
        print >>sys.stderr, __usage__.strip()
        return 1
    # check arguments
    if len(args) != 0:
        print >>sys.stderr, 'Error: no arguments allowed\n'
        print >>sys.stderr, __usage__.strip()
        return 2
    # process options
    history_count = 0
    decode = False
    live = False
    logged = False
    unknown = False
    verbose = 0
    for o, a in opts:
        if o == '--help':
            print __usage__.strip()
            return 0
        elif o in ('-d', '--decode'):
            decode = True
        elif o in ('-h', '--history'):
            history_count = int(a)
        elif o in ('-l', '--live'):
            live = True
            logged = False
        elif o in ('-m', '--logged'):
            live = False
            logged = True
        elif o in ('-u', '--unknown'):
            unknown = True
        elif o in ('-v', '--verbose'):
            verbose += 1
    # do it!
    logger = ApplicationLogger(verbose)
    ws = WeatherStation.weather_station()
    raw_fixed = ws.get_raw_fixed_block()
    if not raw_fixed:
        print "No valid data block found"
        return 3

    meta = {}
    for ptr in range(0x0000, 0x200, 0x20):
        if (ptr >= 0x100):
            raw_dump_better(ptr, raw_fixed[ptr:ptr+0x20])
        else:
            if not(live): raw_dump(ptr, raw_fixed[ptr:ptr+0x20])
            raw_interprete(ptr, raw_fixed[ptr:ptr+0x20], meta)
    current_block = ws._read_custom_block(meta['current'])
    for ptr in range(0x0000, 0x100, 0x20):
        if not(live): raw_dump(meta['current'] + ptr, current_block[ptr:ptr+0x20])
        if not(live): raw_dump_better(meta['current'] + ptr, current_block[ptr:ptr+0x20])
        raw_interprete(meta['current'] + ptr, current_block[ptr:ptr+0x20], meta)
    print "############################"
    print "last record = ", meta['last_record']
    date = datetime.datetime.now().replace(second=0, microsecond=0)
    if live:
        history = []
        l = range(0x100, meta['last_record'], 4)
        l.reverse()
        for ptr in l:#range(meta['last_record'], 0x100, 4):
            data = ws.get_raw_data(ptr)
            res = raw_dump_better(ptr, data, False, True)
            #print "-debug:",  res, len(history), len(l)
            res['date'] = date.strftime("%d/%m %H-%M")
            res['timestamp'] = time.mktime(date.timetuple())
            history.append(res)
            date = date - datetime.timedelta(minutes=res['delay'])
        history.reverse()
        for i in history:
            line=  str(i['timestamp'])+',"'+str(i['date'])+'",'+str(i['temp'])+','+str(i['humidity'])
            print line
    if history_count > 0:
        fixed_block = ws.get_fixed_block()
        print "Recent history"
        ptr = fixed_block['current_pos']
        date = datetime.datetime.now().replace(second=0, microsecond=0)
        for i in range(history_count):
            if decode:
                data = ws.get_data(ptr)
                print date, data
                date = date - datetime.timedelta(minutes=data['delay'])
            else:
                raw_dump(ptr, ws.get_raw_data(ptr))
            ptr = ws.dec_ptr(ptr)
    #~ if live:
        #~ for data, ptr, logged in ws.live_data():
            #~ print "%04x" % ptr,
            #~ print data['idx'].strftime('%H:%M:%S'),
            #~ del data['idx']
            #~ print data
    if logged:
        for data, ptr, logged in ws.live_data(logged_only=True):
            print "%04x" % ptr,
            print data['idx'].strftime('%H:%M:%S'),
            del data['idx']
            print data
    del ws
    return 0
if __name__ == "__main__":
    try:
        sys.exit(main())
    except KeyboardInterrupt:
        pass
